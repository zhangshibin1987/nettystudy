import com.shareyi.netty.nio.NicSocketServer;
import com.shareyi.netty.nio.NioSingleThreadReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.channels.Selector;

/**
 * nio基础的server 测试
 *
 * @author zhangshibin
 * @since 2018/9/10
 */
public class NioSocketServerTest {


    @Test
    public void testNioSocketServer() {
        try {
            Selector selector = Selector.open();
            NicSocketServer nicSocketServer = new NicSocketServer();
            NioSingleThreadReader nioSingleThreadReader = new NioSingleThreadReader(selector);
            //启动异步线程循环读取数据
            new Thread(nioSingleThreadReader).start();
            nicSocketServer.startServer(nioSingleThreadReader,8087);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
