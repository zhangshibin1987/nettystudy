package com.shareyi.netty.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志辅助工具类
 *
 * @author david
 * @since 2017/12/12
 */
public class LogHelper {

    public static final Logger DEFAULT= LoggerFactory.getLogger("defaultLog");
    public static final Logger EXCEPTION= LoggerFactory.getLogger("exceptionLog");

}
